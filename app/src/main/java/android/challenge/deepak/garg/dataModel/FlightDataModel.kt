package android.challenge.deepak.garg.dataModel

import android.challenge.deepak.garg.data.Appendix
import android.challenge.deepak.garg.data.Flight

/**
 * Created by deepak.garg on 18/08/2020
 */

class FlightDataModel private constructor(
    val departureAirportName: String?,
    val arrivalAirportName: String?,
    val flightDuration: Long?,
    val takeOffTime: Long?,
    val landingTime: Long?,
    val travelClass: String?,
    val airlineName: String?,
    val flightFares: List<FlightFareDataModel>?){


    class Builder {

        var departureAirportName: String? = null
            private set
        var arrivalAirportName: String? = null
            private set
        var flightDuration: Long? = null
            private set
        var takeOffTime: Long? = null
            private set
        var landingTime: Long? = null
            private set
        var travelClass: String? = null
            private set
        var airlineName: String? = null
            private set
        var flightFares: List<FlightFareDataModel>? = null
            private set

        constructor(flight: Flight,appendix: Appendix){
            departureAirportName(appendix.airports?.get(flight.departureAirportCode))
            arrivalAirportName(appendix.airports?.get(flight.arrivalAirportCode))
            takeOffTime(flight.departureTime)
            flightDuration(flight.arrivalTime ?: 0 - (flight.departureTime ?: 0))
            landingTime(flight.arrivalTime)
            travelClass(flight.airlineClass)
            airlineName(appendix.airlines?.get(flight.airlineCode))
            flightFares(flight.flightFares?.map { FlightFareDataModel(appendix.providers?.get("" + it.providerId),it.fare) })
        }


        fun departureAirportName(departureAirportName: String?) = apply { this.departureAirportName = departureAirportName }
        fun arrivalAirportName(arrivalAirportName: String?) = apply { this.arrivalAirportName = arrivalAirportName }


        fun flightDuration(flightDuration: Long?) = apply { this.flightDuration = flightDuration }
        fun takeOffTime(takeOffTime: Long?) = apply { this.takeOffTime = takeOffTime }
        fun landingTime(landingTime: Long?) = apply { this.landingTime = landingTime }

        fun travelClass(travelClass: String?) = apply { this.travelClass = travelClass }

        fun airlineName(airlineName: String?) = apply { this.airlineName = airlineName }

        fun flightFares(flightFares: List<FlightFareDataModel>?) = apply { this.flightFares = flightFares }

        fun build() = FlightDataModel(departureAirportName,arrivalAirportName,flightDuration,takeOffTime,landingTime,travelClass,airlineName,flightFares)
    }


}