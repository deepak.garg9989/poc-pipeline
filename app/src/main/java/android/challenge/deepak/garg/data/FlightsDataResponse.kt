package android.challenge.deepak.garg.data

import com.google.gson.annotations.SerializedName

/**
 * Created by deepak.garg on 18/08/2020
 */

class FlightsDataResponse (
    @field:SerializedName("appendix")
    var appendix: Appendix?,
    @field:SerializedName("flights")
    var flights: List<Flight>?
)