package android.challenge.deepak.garg.utils

/**
 * Created by deepak.garg on 17/08/2020
 */

object Constants {
    const val BASE_URL = "http://www.mocky.io/v2/"

    const val GET_FLIGHTS_PATH_PARAMETER = "5979c6731100001e039edcb3"
}