package android.challenge.deepak.garg.data

import com.google.gson.annotations.SerializedName

/**
 * Created by deepak.garg on 18/08/2020
 */
class FlightFare (
    @field:SerializedName("providerId")
    var providerId: Int?,
    @field:SerializedName("fare")
    var fare: Long?
)