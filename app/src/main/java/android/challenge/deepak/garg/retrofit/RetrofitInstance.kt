package android.challenge.deepak.garg.retrofit

import android.challenge.deepak.garg.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by deepak.garg on 17/08/2020
 */
object RetrofitInstance {

    private var retrofit: Retrofit? = null

    private val retrofitInstance: Retrofit
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(OkHttpClient.Builder().build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!
        }

    fun getFlightsService() : GetFlightsService{
        return retrofitInstance.create(GetFlightsService::class.java)
    }
}