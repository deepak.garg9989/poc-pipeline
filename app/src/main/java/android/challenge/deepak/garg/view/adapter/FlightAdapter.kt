package android.challenge.deepak.garg.view.adapter

import android.app.Activity
import android.challenge.deepak.garg.R
import android.challenge.deepak.garg.dataModel.FlightDataModel
import android.challenge.deepak.garg.dataModel.FlightFareDataModel
import android.challenge.deepak.garg.utils.DateUtility
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.flight_fare_row.view.*
import kotlinx.android.synthetic.main.flight_row.view.*

/**
 * Created by deepak.garg on 18/08/2020
 */

class FlightAdapter (var flightsDataModel: List<FlightDataModel>?,val seeAllProvidersListener : (flightFares: List<FlightFareDataModel>?) -> Unit) : RecyclerView.Adapter<FlightAdapter.FlightViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightViewHolder {
        return FlightViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.flight_row, parent, false))
    }

    override fun onBindViewHolder(holder: FlightViewHolder, position: Int) {
       holder.bindData(position)
    }

    override fun getItemCount(): Int {
        return flightsDataModel?.size ?: 0
    }

    fun updateAdapter(flightsDataModelList: List<FlightDataModel>?){
        flightsDataModel = flightsDataModelList
        notifyDataSetChanged()
    }

    inner class FlightViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val airlineName : TextView = view.airlineName
        val departureTime : TextView = view.departureTime
        val departurePlace : TextView = view.departurePlace
        val duration : TextView = view.duration
        val arrivalTime : TextView = view.arrivalTime
        val arrivalPlace : TextView = view.arrivalPlace
        val tvFareProvider : TextView = view.tvFareProvider
        val tvFare : TextView = view.tvFare
        val tvViewOtherProviders : TextView = view.tvViewOtherProviders

        fun bindData(position: Int){
            val flight = flightsDataModel!!.get(position)
            airlineName.setText("${flight.airlineName} (${flight.travelClass})   ${DateUtility.displayDate(flight.takeOffTime)}")
            departurePlace.setText(flight.departureAirportName)
            arrivalPlace.setText(flight.arrivalAirportName)
            departureTime.setText(DateUtility.displayTime(flight.takeOffTime))
            arrivalTime.setText(DateUtility.displayTime(flight.landingTime))
            duration.setText(DateUtility.convertMillisToHourMinuteSeconds(flight.landingTime!! - flight.takeOffTime!!))
            if(flight.flightFares != null && !flight.flightFares.isEmpty()) {
                val flightFare = flight.flightFares.sortedBy { it.fare }.get(0)
                tvFareProvider.setText(flightFare.providerName)
                tvFare.setText("\u20B9 " + flightFare.fare)
                if(flight.flightFares.size > 1){
                    tvViewOtherProviders.visibility = View.VISIBLE
                }else{
                    tvViewOtherProviders.visibility = View.GONE
                }
            }else{
                tvViewOtherProviders.visibility = View.GONE
            }
            tvViewOtherProviders.setOnClickListener {
                seeAllProvidersListener(flight.flightFares)
            }
        }

    }
}