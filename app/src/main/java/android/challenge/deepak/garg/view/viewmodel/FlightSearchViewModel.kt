package android.challenge.deepak.garg.view.viewmodel

import android.challenge.deepak.garg.data.FlightDataStatus
import android.challenge.deepak.garg.data.SortingType
import android.challenge.deepak.garg.dataModel.FlightDataModel
import android.challenge.deepak.garg.repository.RemoteRepository
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by deepak.garg on 17/08/2020
 */

class FlightSearchViewModel : ViewModel(){

    val flightsDataLiveData = MutableLiveData<FlightDataStatus>()

    var flightsData : List<FlightDataModel>? = null

    //Keeping sortingType in viewModel prevents it to lost after rotation of device
    var sortingType = SortingType.FARE

    val courotineExceptionHandler = CoroutineExceptionHandler { exception, _ ->
        flightsDataLiveData.postValue(FlightDataStatus.ERROR)
    }

    fun getFlightsInfo() {
        viewModelScope.launch(Dispatchers.IO + courotineExceptionHandler) {
            val flightDataResponse = RemoteRepository.getFlightData()
            if (flightDataResponse.isSuccessful() && flightDataResponse.body() != null){
                if (flightDataResponse.body()!!.flights != null
                    && flightDataResponse.body()!!.flights!!.isNotEmpty()
                    && flightDataResponse.body()!!.appendix != null
                ) {
                    flightsData = flightDataResponse.body()!!.flights!!.map {
                        FlightDataModel.Builder(
                            it,
                            flightDataResponse.body()!!.appendix!!
                        ).build()
                    }
                    flightsDataLiveData.postValue(FlightDataStatus.SUCCESS(getSortedFlightData(flightsData!!)))
                } else {
                    flightsDataLiveData.postValue(FlightDataStatus.SUCCESS(null))
                }
            } else {
                flightsDataLiveData.postValue(FlightDataStatus.ERROR)
            }
        }
    }

    fun sortData(sortingType: SortingType){
        this.sortingType = sortingType
        viewModelScope.launch(Dispatchers.Default) {
            if (flightsData != null) {
                flightsDataLiveData.postValue(FlightDataStatus.SUCCESS(getSortedFlightData(flightsData!!)))
            } else {
                flightsDataLiveData.postValue(FlightDataStatus.ERROR)
            }
        }
    }

    fun getSortedFlightData(flightsDataModel: List<FlightDataModel>) : List<FlightDataModel>{
        when(sortingType){
            SortingType.FARE ->{
                return flightsDataModel.sortedBy { it.flightFares!!.map { it.fare }.minBy { it!!.toLong() } }
            }
            SortingType.DEPARTURE_TIME ->{
                return flightsDataModel.sortedBy { it.takeOffTime }
            }
            SortingType.LANDING_TIME ->{
                return flightsDataModel.sortedBy { it.landingTime }
            }
        }
    }
}