package android.challenge.deepak.garg.utils

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by deepak.garg on 19/08/2020
 */

object DateUtility {
    private val DISPLAY_TIME_FORMAT = "hh:mm a"
    private val DISPLAY_DATE_FORMAT = "dd MMM "

    fun displayTime(time: Long?): String {
        if (time != null) {
            val calender = Calendar.getInstance()
            calender.timeInMillis = time
            return SimpleDateFormat(DISPLAY_TIME_FORMAT, Locale.US).format(calender.time)
        } else {
            return ""
        }
    }

    fun displayDate(time: Long?): String {
        if (time != null) {
            val calender = Calendar.getInstance()
            calender.timeInMillis = time
            return SimpleDateFormat(DISPLAY_DATE_FORMAT, Locale.US).format(calender.time)
        } else {
            return ""
        }
    }

    fun convertMillisToHourMinuteSeconds(millis: Long): String {
        var hms = ""
        try {
            val format = "%02dh:%02dm"
            hms = String.format(
                format, TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1)
            )
        } catch (ex: Exception) {
        }
        return hms
    }

}