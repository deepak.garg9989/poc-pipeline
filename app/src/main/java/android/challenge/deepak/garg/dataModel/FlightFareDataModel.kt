package android.challenge.deepak.garg.dataModel

/**
 * Created by deepak.garg on 18/08/2020
 */
class FlightFareDataModel (
    var providerName : String? = null,
    var fare : Long? = null
)