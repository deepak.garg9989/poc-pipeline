package android.challenge.deepak.garg.data

import com.google.gson.annotations.SerializedName

/**
 * Created by deepak.garg on 17/08/2020
 */

class Flight (
    @field:SerializedName("originCode")
    var departureAirportCode: String?,
    @field:SerializedName("destinationCode")
    var arrivalAirportCode: String?,
    @field:SerializedName("departureTime")
    var departureTime: Long?,
    @field:SerializedName("arrivalTime")
    var arrivalTime: Long?,
    @field:SerializedName("fares")
    var flightFares: List<FlightFare>?,
    @field:SerializedName("airlineCode")
    var airlineCode: String?,
    @field:SerializedName("class")
    var airlineClass: String?
)