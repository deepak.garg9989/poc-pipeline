package android.challenge.deepak.garg.data

import android.challenge.deepak.garg.dataModel.FlightDataModel

/**
 * Created by deepak.garg on 18/08/2020
 */

sealed class FlightDataStatus {
    object ERROR : FlightDataStatus()
    data class SUCCESS(val flightsDataModel: List<FlightDataModel>?) :  FlightDataStatus()
}