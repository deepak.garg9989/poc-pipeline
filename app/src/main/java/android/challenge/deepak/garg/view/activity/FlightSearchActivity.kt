package android.challenge.deepak.garg.view.activity

import android.challenge.deepak.garg.R
import android.challenge.deepak.garg.data.FlightDataStatus
import android.challenge.deepak.garg.utils.ViewUtils
import android.challenge.deepak.garg.view.adapter.FlightAdapter
import android.challenge.deepak.garg.view.viewmodel.FlightSearchViewModel
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.PopupWindow
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class FlightSearchActivity : AppCompatActivity(),View.OnClickListener {

    private lateinit var flightSearchViewModel: FlightSearchViewModel
    private var flightAdapter: FlightAdapter? = null
    private var popupWindow : PopupWindow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeViewModel()
        progressBar.visibility = View.VISIBLE
        recyclerView.layoutManager = LinearLayoutManager(this)
        setSwipeRefereshListener()
        cvSort.setOnClickListener(this)
        tv_sort_by_value.setText(flightSearchViewModel.sortingType.getDisplayName())
        if(flightSearchViewModel.flightsData == null) {
            flightSearchViewModel.getFlightsInfo()
        }
    }

    private fun initializeViewModel() {
        flightSearchViewModel = ViewModelProviders.of(this).get(FlightSearchViewModel::class.java)
        flightSearchViewModel.flightsDataLiveData.observe(this, Observer {
            progressBar.visibility = View.GONE
            if (swipeRefreshLayout.isRefreshing) {
                swipeRefreshLayout.isRefreshing = false
            }
            when (it) {
                is FlightDataStatus.ERROR -> {
                    handleFailure()
                }
                is FlightDataStatus.SUCCESS -> {
                    handleSuccess(it)
                }
            }
        })
    }

    //call back if user refreshes,fetch latest flights data
    private fun setSwipeRefereshListener() {
        swipeRefreshLayout.setOnRefreshListener {
            flightSearchViewModel.getFlightsInfo()
        }
    }

    //failure in getting data for flights
    private fun handleFailure() {
        tvEmpty.visibility = View.VISIBLE
        tvEmpty.text = "Something went wrong.Please swipe to refresh"
        recyclerView.visibility = View.GONE
        cvSort.visibility = View.GONE
    }

    private fun handleSuccess(flightDataStatus: FlightDataStatus.SUCCESS) {
        if (flightDataStatus.flightsDataModel != null && !flightDataStatus.flightsDataModel.isEmpty()) {
            tvEmpty.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            cvSort.visibility = View.VISIBLE
        } else {
            tvEmpty.visibility = View.VISIBLE
            tvEmpty.text = "No Flights found"
            recyclerView.visibility = View.GONE
            //If no flights are found,do not show sorting button
            cvSort.visibility = View.GONE
        }
        if (flightAdapter == null) {
            flightAdapter = FlightAdapter(flightDataStatus.flightsDataModel){
                popupWindow = ViewUtils.seeAllProvidersPopup(it!!,this)
            }
            recyclerView.adapter = flightAdapter
        } else {
            flightAdapter!!.updateAdapter(flightDataStatus.flightsDataModel)
        }
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.cvSort ->{
                ViewUtils.openSortingPopup(this,flightSearchViewModel.sortingType){
                    tv_sort_by_value.setText(it.getDisplayName())
                    progressBar.visibility = View.VISIBLE
                    flightSearchViewModel.sortData(it)
                }
            }
        }
    }

    override fun onBackPressed() {
        if (popupWindow != null && popupWindow!!.isShowing) {
            popupWindow!!.dismiss()
        } else {
            super.onBackPressed()
        }
    }
}
