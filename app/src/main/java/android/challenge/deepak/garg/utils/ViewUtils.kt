package android.challenge.deepak.garg.utils

import android.app.Activity
import android.challenge.deepak.garg.R
import android.challenge.deepak.garg.data.SortingType
import android.challenge.deepak.garg.dataModel.FlightFareDataModel
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.sort_row.view.*
import kotlinx.android.synthetic.main.sorting_popup.view.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.flight_fare_row.view.*
import kotlinx.android.synthetic.main.providers_popup.view.*


/**
 * Created by deepak.garg on 19/08/2020
 */

object ViewUtils {

    /**
     * @param context Activity context
     * @param currentSortType previous selected sorting parameter
    **/
    fun openSortingPopup(context : Activity,currentSortType : SortingType, sortClickListener : (selectedSortType : SortingType) -> Unit ){
        val mBottomSheetDialog = BottomSheetDialog(context)
        val sheetView = context.getLayoutInflater().inflate(R.layout.sorting_popup, null)
        sheetView.iv_close_sort_by.setOnClickListener {
            if(mBottomSheetDialog.isShowing){
                mBottomSheetDialog.dismiss()
            }
        }
        sheetView.llSortType.removeAllViews()
        for(sortingType in SortingType.values()){
            val view = context.getLayoutInflater().inflate(R.layout.sort_row, null)
            view.tv_sort_heading.setText(sortingType.getDisplayName())
            if(sortingType == currentSortType){
                view.iv_sort_by_joiningFees_check.visibility = View.VISIBLE
            }
            view.setOnClickListener {
                if(sortingType != currentSortType) {
                    sortClickListener(sortingType)
                }
                mBottomSheetDialog.dismiss()
            }
            sheetView.llSortType.addView(view)
        }
        mBottomSheetDialog.setContentView(sheetView)
        val mBehavior = BottomSheetBehavior.from(sheetView.getParent() as View)

        mBottomSheetDialog.setOnShowListener({ dialogInterface ->
            mBehavior.peekHeight = sheetView.getHeight() //get the height dynamically
        })
        mBottomSheetDialog.show()
    }

    fun seeAllProvidersPopup(flightFares: List<FlightFareDataModel>, context: Activity): PopupWindow? {
        if (flightFares.isEmpty()) {
            return null
        }
        val layoutInflater = LayoutInflater.from(context)
        val popupView = layoutInflater.inflate(R.layout.providers_popup, null)
        //Add each fare to Linear layout
        for (fares in flightFares) {
            val view = context.getLayoutInflater().inflate(R.layout.flight_fare_row, null)
            view.tvProviderName.setText(fares.providerName)
            view.tvProviderFare.setText("\u20B9 " + fares.fare)
            popupView.llFares.addView(view)
        }
        val popupWindow = PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        popupWindow.setOutsideTouchable(true)
        popupWindow.setFocusable(false)
        popupWindow.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.yellow_fffae3))
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0)
        popupWindow.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        return popupWindow
    }

}