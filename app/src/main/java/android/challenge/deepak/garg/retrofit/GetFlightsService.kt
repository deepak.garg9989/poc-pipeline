package android.challenge.deepak.garg.retrofit

import android.challenge.deepak.garg.data.FlightsDataResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by deepak.garg on 17/08/2020
 */

interface GetFlightsService {

    @GET("{path}")
    suspend fun getFlightData(@Path("path") path : String): Response<FlightsDataResponse>
}