package android.challenge.deepak.garg.data

/**
 * Created by deepak.garg on 18/08/2020
 */

enum class SortingType {
    FARE {
        override fun getDisplayName(): String = "Cheapest"
    },
    DEPARTURE_TIME {
        override fun getDisplayName(): String = "Take-off Time"
    },
    LANDING_TIME {
        override fun getDisplayName(): String = "Landing Time"
    };

    abstract fun getDisplayName(): String
}