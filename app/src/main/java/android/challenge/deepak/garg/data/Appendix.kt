package android.challenge.deepak.garg.data

import com.google.gson.annotations.SerializedName

/**
 * Created by deepak.garg on 18/08/2020
 */
class Appendix (
    @field:SerializedName("airlines")
    var airlines: HashMap<String,String>?,
    @field:SerializedName("airports")
    var airports: HashMap<String,String>?,
    @field:SerializedName("providers")
    var providers: HashMap<String,String>?
)